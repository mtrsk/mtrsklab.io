# mtrsk.gitlab.io

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Source code for my Hakyll blog as a [gitlab page](https://mtrsk.gitlab.io/).

## Setup

```
$ make mathjax-setup
$ make bulma-setup
```

## Building

* With Nix
    ```
    $ nix-build
    ```

## Running it locally

```
$ make watch
```

## Using the CLI

There `blog` command can be used to create new posts. As of now it's still a work in progress.

```
$ blog
Missing: (-a|--author ARG) (-t|--title ARG)

Usage: blog (-a|--author ARG) (-t|--title ARG)
  -a NAME -t TITLE
```

## Deploy

```
$ ./deploy.hs
```
