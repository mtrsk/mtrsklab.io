#! /usr/bin/env nix-shell
#! nix-shell -i runghc -p "haskellPackages.ghcWithPackages (p: [p.turtle])"
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

import           Turtle

type Username = Text

type Branch = Text

type Remote = Text

data Command =
  Up UpOptions
  deriving (Show)

data UpOptions =
  UpOptions
    { remote :: Remote
    , branch :: Branch
    }
  deriving (Show)

data GitConf =
  GitConf
    { username  :: Username
    , gitBranch :: Branch
    , gitRemote :: Remote
    }
  deriving (Show)

defaultGitlab =
  GitConf
    { username = "mtrsk"
    , gitBranch = "master"
    , gitRemote = "git@gitlab.com:mtrsk/mtrsk.gitlab.io.git"
    }

defaultGithub =
  GitConf
    { username = "mtrsk"
    , gitBranch = "master"
    , gitRemote = "git@github.com:mtrsk/mtrsk.github.io.git"
    }

parser :: Parser Command
parser = fmap Up (subcommand "up" "Default deploy" upParser)

upParser :: Parser UpOptions
upParser =
  UpOptions <$> argText "remote" "Remote GIT url" <*>
  argText "branch" "Remote GIT branch"

description :: Description
description = "Deploy script for both Gitlab and Github pages"

push :: MonadIO m => Text -> Text -> m ()
push origin master = do
  status <- proc "git" ["push", origin, master] empty
  case status of
    ExitSuccess -> return ()
    ExitFailure n ->
      die
        ("push " <>
         origin <> " " <> master <> " failed with exit code: " <> repr n)

main :: IO ()
main = do
  x <- options description parser
  case x of
    Up UpOptions {..} -> do
      let Just remoteToLine = textToLine remote
      let Just branchToLine = textToLine branch
      echo $ "Deploying to " <> remoteToLine <> " on " <> branchToLine
      push remote branch
