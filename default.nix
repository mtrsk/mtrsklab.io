{ pkgs ? import ./pinned-nixpkgs.nix {} }:

let
  gitignoreSource = import ./gitignore.nix {};
in
pkgs.haskellPackages.callCabal2nix "blog" (gitignoreSource ./.) {}
