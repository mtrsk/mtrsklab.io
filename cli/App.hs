{-# LANGUAGE ScopedTypeVariables #-}

module App where

import           Data.Semigroup      ((<>))
import           FileOps             (createPost)
import           Options.Applicative

type CmdArgs = IO [String]

data Options =
  Options
    { author :: String
    , title  :: String
    }

opt :: Parser Options
opt =
  Options <$>
  strOption (long "author" <> short 'a' <> help "name of the author") <*>
  strOption (long "title" <> short 't' <> help "title of the post")

cmds :: Options -> IO ()
cmds (Options author title) = createPost author title []

main :: IO ()
main = cmds =<< execParser opts
  where
    opts =
      info
        (opt <**> helper)
        (fullDesc <>
         progDesc "-a NAME -t TITLE" <>
         header "app-utils - Some utils for writing blog posts on Hakyll")
