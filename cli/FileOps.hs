{-# LANGUAGE OverloadedStrings #-}

module FileOps where

import           Data.Char
import           Data.List           (intercalate)
import           Data.Time           (defaultTimeLocale, formatTime)
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.LocalTime
import           System.Directory
import           System.Environment
import           System.Exit
import           System.IO
import           System.Process
import qualified Turtle

type Title = String

type Author = String

type Keywords = [String]

postsDir :: FilePath
postsDir = "/posts/"

defaultContent :: Title -> Author -> Keywords -> String
defaultContent title author keywords =
  intercalate
    "\n"
    [ "---"
    , "title: " ++ title
    , "author: " ++ author
    , "keywords: " ++ intercalate ", " keywords
    , "---"
    ]

normalize :: String -> [String]
normalize = words . map toLower

mkFileName :: [String] -> String
mkFileName xs = intercalate "-" xs ++ ".md"

creationTime :: IO String
creationTime = do
  now <- getCurrentTime
  timezone <- getCurrentTimeZone
  let znow = utcToLocalTime timezone now
  let f = formatTime defaultTimeLocale "%Y-%m-%d" znow
  return f

createPost :: Author -> Title -> Keywords -> IO ()
createPost author title keywords = do
  d <- getCurrentDirectory
  time <- creationTime
  let nargs = map toLower $ intercalate "-" (normalize title)
  let filename = mkFileName [time, nargs]
  writeFile (d ++ postsDir ++ filename) $ defaultContent title author keywords
