{-# LANGUAGE OverloadedStrings #-}

module BulmaTemplate where

import           Control.Monad               (forM_)
import           Data.List                   (intersperse)
import           Text.Blaze.Html5            as H
import           Text.Blaze.Html5.Attributes as A

import           Text.Blaze                  (toMarkup, toValue)

defaultMain :: H.Html
defaultMain =
  H.main $ do
    H.h1 "$title$"
    "$body$"
