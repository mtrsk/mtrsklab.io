---
title: I've Switched to Nixos
author: Marcos
keywords: NixOS, Linux
---

After quite some time being a Debian user I've decided to become a full time meme distro user. At first I was following some of the manuals in an attempt to recreate my config in virtualbox. A day later this distro seemed too cool not to try it out for real, so why not?

</br>

It's kinda weird to adapt, in Debian I was accostumed to just run a command here and there and have a bunch of config files across my system, with [Nix](https://nixos.org/nix/) (the configuration language) one can have a single file (`configuration.nix`) to manager everything and, since Nix is also a programming language, you can modularize it to make things easier to manage. NixOS is also quite forgiving when it comes to experimenting, you can always rollback your entire system to a previous generation if something explodes. The `#nixos` community on both freenode and [discourse](https://discourse.nixos.org/) is also very helpful.

</br>

If you wanna see my crappy config just take a look [here](https://github.com/mtrsk/nixos-config).
