let
  dev-env = (import ./default.nix {}).env;
  pkgs = import ./pinned-nixpkgs.nix {};
  haskellPkgs = with pkgs; [
    cabal-install
    git
    hlint
    haskellPackages.hindent
    haskellPackages.ghcid
  ];
in
  dev-env.overrideAttrs (old : rec {
    buildInputs = old.buildInputs ++ haskellPkgs;
    shellHook = ''
      export GIT_SSL_CAINFO=/etc/ssl/certs/ca-bundle.crt
      export LOCAL_BUILD=1

      export PATH="$PWD/result/bin/:$PATH"
    '';
  })
