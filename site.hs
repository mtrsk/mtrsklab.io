{-# LANGUAGE OverloadedStrings #-}

import           BulmaTemplate
import qualified Data.Map                 as M
import           Data.Monoid              (mappend)
import qualified Data.Set                 as S
import           Hakyll
import           Text.Pandoc.Highlighting
import           Text.Pandoc.Options

--------------------------------------------------------------------------------
-- Contexts
--------------------------------------------------------------------------------
postCtx :: Context String
postCtx = dateField "date" "%Y, %B %e" `mappend` defaultContext

indexCtx :: [Item String] -> Context String
indexCtx posts =
  listField "posts" postCtx (return posts) `mappend` constField "title" "Home" `mappend`
  defaultContext

--------------------------------------------------------------------------------
-- Config
--------------------------------------------------------------------------------
config :: Configuration
config = defaultConfiguration {destinationDirectory = "public"}

-- Pandoc Extensions for MathJax & Code Highlight
syntaxHighlightingStyle :: Style
syntaxHighlightingStyle = haddock

-- Custom compiler
pandocCompiler_ :: Compiler (Item String)
pandocCompiler_ =
  let mathExtensions =
        [Ext_tex_math_dollars, Ext_tex_math_double_backslash, Ext_latex_macros]
      codeExtensions =
        [ Ext_fenced_code_blocks
        , Ext_backtick_code_blocks
        , Ext_fenced_code_attributes
        ]
      newExtensions =
        foldr
          enableExtension
          defaultExtensions
          (mathExtensions <> codeExtensions)
      defaultExtensions = writerExtensions defaultHakyllWriterOptions
      writerOptions =
        defaultHakyllWriterOptions
          { writerExtensions = newExtensions
          , writerHTMLMathMethod = MathJax ""
          , writerHighlightStyle = Just syntaxHighlightingStyle
          }
   in pandocCompilerWith defaultHakyllReaderOptions writerOptions

generateSyntaxHighlightingCSS :: IO ()
generateSyntaxHighlightingCSS = do
  let css = styleToCss syntaxHighlightingStyle
  writeFile "css/syntax.css" css
  return ()

--------------------------------------------------------------------------------
-- Main
--------------------------------------------------------------------------------
main :: IO ()
main = do
  generateSyntaxHighlightingCSS
  hakyllWith config $ do
    match "images/*" $ do
      route idRoute
      compile copyFileCompiler
    match "css/*" $ do
      route idRoute
      compile compressCssCompiler
    match "js/*" $ do
      route idRoute
      compile copyFileCompiler
    match "posts/*" $ do
      route $ setExtension "html"
      compile $
        pandocCompiler_ >>= loadAndApplyTemplate "templates/post.html" postCtx >>=
        loadAndApplyTemplate "templates/default.html" postCtx >>=
        relativizeUrls
    create ["archive.html"] $ do
      route idRoute
      compile $ do
        posts <- recentFirst =<< loadAll "posts/*"
        let archiveCtx =
              listField "posts" postCtx (return posts) `mappend`
              constField "title" "Archives" `mappend`
              defaultContext
        makeItem "" >>= loadAndApplyTemplate "templates/archive.html" archiveCtx >>=
          loadAndApplyTemplate "templates/default.html" archiveCtx >>=
          relativizeUrls
    match "index.html" $ do
      route idRoute
      compile $ do
        posts <- recentFirst =<< loadAll "posts/*"
        let indexCtx =
              listField "posts" postCtx (return posts) `mappend`
              constField "title" "Home" `mappend`
              defaultContext
        getResourceBody >>= applyAsTemplate indexCtx >>=
          loadAndApplyTemplate "templates/default.html" indexCtx >>=
          relativizeUrls
    match "templates/*" $ compile templateBodyCompiler
