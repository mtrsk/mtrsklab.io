ifeq ($(LOCAL_BUILD),1)
	# Trouble running curl with SSL on a pure shell
	CURL_FLAGS=--insecure
else
	CURL_FLAGS=
endif
$(info Make: FLAGS="$(CURL_FLAGS)".)

build:
	nix-build

clean:
	./result/bin/site clean

watch:
	./result/bin/site watch -v

site:
	./result/bin/site clean
	./result/bin/site build

setup:
	git submodule init

shell:
	nix-shell shell.nix

shell-pure:
	nix-shell --pure shell.nix

.PHONY: run repl shell shell-pure mathjax-setup bulma-setup fa-setup
